# Holly Cui's Personal Zola Site 🪐

## Description
Hello there! Welcome to my personal site built by [Zola](https://www.getzola.org/), a static site generator written in Rust and imported with Tera template engine. The demo site is hosted by GitLab where you can directly access [HERE](https://hollycui-website-hollyyfc-6a8253d86e5dae06d0b417082cf7eabe56b77.gitlab.io/) and get a brief overview of me and my background. 

## Roadmap
This site adopted a cool animated theme [particle](https://www.getzola.org/themes/particle/) which is created by [Silvano Sallese](https://github.com/svavs) under the MIT license and listed on Zola's official theme page. A rough sketch of the site structure is as follows: 

- **Home / Landing Page**: A giant animated welcome page with my name, contact info, and media. 
- **Introduction**: A section where texts, embedded links, and Emojis are combined to give a brief intro of my previous study and work experiences. 
- **Skills**: A tab where my technical skills are detailed with programming tools icons. 
- **Projects**: A tab with my previous project highlights. Links to project details and images are also added to ensure visual experience and user journey. 

Due to the structure of the pre-defined theme, the site does not have an explicit menu bar or navbar. The two tabs, `Skills` and `Projects`, are instead sections with different macro features, CSS, and HTML structures. Feel free to explore my modifications and customizations to the structural codes above!

## Project status 🔴
Unfortunately, the `particle` theme **does not support with higher versions of Zola** at this stage. The [issue](https://github.com/svavs/particle-zola/issues/11) on the theme's GitHub repo has alarmed the incompatibility to the author. However, it seems like the author is still working on fixing and porting the components to higher Zola without further updates. Thus, my current website hosted by GitLab is not built correctly. (You might see a blank site with mere info modified, but feel free to check my code above as they worked perfectly fine locally and passed all CI/CD checks leading to the successful page building.) 

To give you a sense of what it should be like, here is a [quick demo](https://youtu.be/98p_Lz01g9w) of my site. I will try my best to contact the author and hopefully things will start rolling again soon! Meanwhile, feel free to check out my main portfolio site [HERE](https://hollyyfc.github.io/) built with Ruby and Jekyll theme [So Simple](https://github.com/mmistakes/so-simple-theme).







