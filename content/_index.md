+++
theme = "index.html"
+++


I am an aspiring data scientist and Master's student in the Department of Statistical Science 
at Duke University, where I also obtained my Bachelor's in Statistical Science (Data Science Concentration) and a Minor in Computer Science. My academic journey has been marked by a deep research commitment to statistical analysis, machine learning, and data science, with special focuses on natural language processing, Bayesian statistics, and creative data visualizations. Take a look of my previous [research](https://hollyyfc.github.io/research/) and [intern projects](https://hollyyfc.github.io/projects/) in R and Python, and let me know if you are interested! [👉View My Resume](https://hollyyfc.github.io/Yifan%20(Holly)%20Cui%20Resume.pdf)

Beyond academia, I was an active member of the Duke community, contributing as a Project Manager & Data Analyst in the [Duke Impact Investing Group](https://www.linkedin.com/company/diig/) and as the Chief Technology Officer in the [Duke Statistical Science Majors Union](https://dukegroups.com/ssmu/home/). Additionally, I have been a teaching assistant with 3+ years of experience. Feel free to reach out for project advice and business case studies. 

In my free time, I balance my academic pursuits with diverse interests such as boxing, cycling, and music producing. I was also a board member of [UCSB International Student Advisory Board](https://internationalvistas.blogspot.com/) advocating for international students' rights. Read more and get engaged!

## 🏫 Education
- M.S. Student, Statistics || <strong>Duke University</strong> (*May 2025* )
- B.S., Statistical Science (Data Science Concentration) & Minor in Computer Science || **Duke University** (*May 2023* )
- Statistics and Data Science || University of California, Santa Barbara <br>(Transfer Out on *June 2021* )

## 👩‍💻 Experience Highlights

#### Student Research Affiliate @ [Duke AI Health](https://aihealth.duke.edu/) (_May 2022_ – _Dec 2022_)
<ol>
    <em>Machine Learning in Healthcare: Lab Test Harmonization</em>
</ol>

- Selected as the sole undergraduate amidst a competitive pool of professional candidates for Duke AI Health 2022 cohort, and earned the prestigious opportunity to present research findings at [Duke AI Health Poster Showcase 2022](https://aihealth.duke.edu/poster-showcase-2022/)
- Optimized lab test deduplication of grouper labels by adopting and fine-tuning Bio-BERT NLP structure pre-trained on biomedical corpora; created a new method of cross-comparison similarity evaluation based on ground-truth text embeddings, and uncovered 95% performance boost in the application to Duke lab *analyte* database

#### Data Science Intern @ [Hiya](https://www.hiya.com/) (_May 2022_ – _Aug 2022_)
<ol>
    <em>Hiya Shield Project: Robocall Identification & Screening</em>
</ol>

- Spearheaded a robocall screening process using NLP text embeddings to determine if an audio sample (or its transcript) is from a known robocall database 
- Quantified the relationship between audio duration and performance of robocall classification; identified the preferred audio truncation length and optimal similarity threshold, and achieved a 67% acceleration in user experience with the introduction of a customizable screening accuracy feature for Hiya mobile App 

#### Lead Author & Research Assistant @ Tsinghua University (_Jun 2020_ – _Mar 2021_)
<ol>
    <em>Cross-Media Retrieval Based on Big Data Technology</em>
</ol>

- Improved performance of permutation invariant training with mean squared error loss through BLSTM/LSTM and CNN in a key media separation technique; proved the improvement in two separation methods – the FIX strategy and the masking-based data augmentation strategy  –  and subsequently developed independent research project
- Paper Publication: [*Audio-Visual Single-Channel Signal Separation based on Big Data Augmentation*](https://ieeexplore.ieee.org/document/9332362) published by IEEE during International Conference on Computer Networks and Electronic Communications (ICCNEC 2020)
